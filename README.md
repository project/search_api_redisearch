# Search API Redisearch

Drupal 8 module for [Search API](https://www.drupal.org/project/search_api) integration with [RediSearch - Redis Powered Search Engine](https://oss.redislabs.com/redisearch/).

## Getting Started

To start using RediSearch as search backend, use Search API configuration menu to create a new search server with "redis" backend option. Any index created on this backend will use RediSearch to index content and perform searching.

### Prerequisites

This module requires:
* Drupal 8 search_api module
* PHP Redisearch backend module

### Installing

```composer require drupal/search_api_redisearch```
