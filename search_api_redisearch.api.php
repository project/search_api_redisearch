<?php

/**
 * @file
 * The api of search_api_redisearch module.
 */

/**
 * Enable additional redisearch dictionaries in spell checking through search api hook.
 *
 * @example
 * function hook_search_api_query_alter(\Drupal\search_api\Query\QueryInterface &$query) {
 *   $dispatcher = Drupal::service('event_dispatcher');
 *   $event = new Drupal\search_api_redisearch\Event\DictionaryEvent('example_dict');
 *   $dispatcher->dispatch('search_api_redisearch.dictionary_event', $event);
 * }
 */
