<?php

namespace Drupal\search_api_redisearch\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\search_api_redisearch\Event\DictionaryEvent;
use Drupal\search_api_redisearch\Plugin\search_api\backend\RediSearch;

/**
 * Class DictionaryEventSubscriber.
 */
class DictionaryEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new DictionaryEventSubscriber object.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [DictionaryEvent::EVENT_NAME => "onNewDictionary"];
  }

  /**
   * Handles DictionaryEvent.
   */
  public function onNewDictionary($dictionary) {
    $name = $dictionary->getDictionaryName();
    RediSearch::addDictionary($name);
  }

}
