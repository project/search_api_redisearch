<?php

namespace Drupal\search_api_redisearch\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * The dictionary event class.
 */
class DictionaryEvent extends Event {

  const EVENT_NAME = 'search_api_redisearch.dictionary_event';

  /**
   * The name of the existing redisearch dictionary.
   *
   * @var string
   */
  protected $dictionaryName;

  /**
   * Constructs an dictionary event object.
   *
   * @param string $dictionaryName
   *   The name of the dictionary.
   */
  public function __construct($dictionaryName) {
    $this->dictionaryName = $dictionaryName;
  }

  /**
   * Get the name of the dictionary.
   *
   * @return string
   *   The name of the dictionary.
   */
  public function getDictionaryName() {
    return $this->dictionaryName;
  }

}
