<?php

namespace Drupal\search_api_redisearch\Event;

use Symfony\Component\EventDispatcher\Event;
use FKRediSearch\Query\QueryBuilder;

/**
 * The search query event class.
 */
class BeforeSearchEvent extends Event {

  const EVENT_NAME = 'search_api_redisearch.before_search_event';

  /**
   * The query object.
   *
   * @var QueryBuilder
   */
  protected $query;

  /**
   * Constructs the event.
   *
   * @param QueryBuilder $query
   *   The query object about to be ran by RediSearch.
   */
  public function __construct(QueryBuilder $query) {
    $this->query = $query;
  }

  /**
   * Getter for the query object.
   *
   * @return QueryBuilder
   *   The query object.
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Setter for the query object
   *
   * @param QueryBuilder $query
   *   The query object.
   */
  public function setQuery(QueryBuilder $query) {
    $this->query = $query;
  }

}
