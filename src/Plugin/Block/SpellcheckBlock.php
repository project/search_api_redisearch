<?php

namespace Drupal\search_api_redisearch\Plugin\block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\search_api\Query\ResultSet;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements spellcheck suggestion block.
 *
 * @Block(
 *  id = "spellcheck_block",
 *  admin_label = @Translation("Spellcheck suggestions"),
 * )
 */
class SpellcheckBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['info'] = [
      '#markup' => t('<b>Note that you need to also enable spellchecking in redisearch backend configuration</b>'),
      '#weight' => -100,
    ];

    $form['search_query_argument'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search query argument'),
      '#description' => $this->t('Name of the argument which holds the search terms in url query. Leave empty to use the last part of the url path instead (so called "clean urls")'),
      '#maxlength' => 32,
      '#default_value' => $this->configuration['search_query_argument'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['search_query_argument'] = $form_state->getValue('search_query_argument');
  }

  /**
   * Gets the search results data using search api query helper service.
   *
   * @return ResultSet|null
   *   search api results
   */
  protected function getSearchData() {
    $container = \Drupal::getContainer();
    $query_helper = $container->get('search_api.query_helper');
    $results = $query_helper->getAllResults();
    $results = array_filter($results, function ($el) {
      return $el instanceof ResultSet;
    });

    if (count($results) > 0) {
      return reset($results);
    }
    else {
      return NULL;
    }
  }

  /**
   * Gets the current query parameters.
   *
   * @return array
   *   Key value of parameters.
   */
  protected function getCurrentQuery() {
    return \Drupal::request()->query->all();
  }

  /**
   * Gets the suggestion link.
   *
   * @param string $new_keys
   *   The suggestion.
   *
   * @return Link
   *   The suggestion link.
   */
  protected function getSuggestionLink($new_keys) {
    $filter_name = trim($this->configuration['search_query_argument']);

    if (is_string($filter_name) && strlen($filter_name) > 0) {
      $url = Url::fromRoute(
        '<current>',
        [$filter_name => $new_keys] + $this->getCurrentQuery()
      );
    }
    else {
      $request = \Drupal::request();
      $url = \Drupal::request()->getPathInfo();
      $url = preg_replace('/(.*\/).*?$/', '\\1' . $new_keys, $url);
      $url = Url::fromUserInput($url, ['query' => $this->getCurrentQuery()]);
    }

    return Link::fromTextAndUrl($new_keys, $url);
  }

  /**
   * Parse suggestions contained in search api result's extra data.
   *
   * @param ResultSet $results
   *   Search api result set.
   *
   * @return string|null
   *   Parsed suggestion string or empty value on no suggestion.
   */
  protected function getSpellcheckSuggestions(ResultSet $results) {
    $keys = $results->getQuery()->getOriginalKeys();
    $new_keys = NULL;
    $suggestions = $results->getExtraData('search_api_spellcheck');

    if (isset($suggestions['collation'])) {
      $new_keys = $suggestions['collation'];
    }
    return $keys !== $new_keys ? $new_keys : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $results = $this->getSearchData();
    if ($results instanceof ResultSet) {
      // Do not show empty suggestions.
      if ($keys = $this->getSpellcheckSuggestions($results)) {
        return [
          '#theme' => 'spellcheck_block_did_you_mean',
          '#link' => $this->getSuggestionLink($keys),
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Avoid caching - search results should not be cached.
    return 0;
  }

}
