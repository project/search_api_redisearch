<?php

namespace Drupal\search_api_redisearch\Plugin\search_api\backend;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use FKRediSearch\Document;
use FKRediSearch\Index;
use Drupal\search_api_redisearch\Utility\QueryBuilder;
use FKRediSearch\Query\Query;
use FKRediSearch\Setup;
use Drupal\search_api_page\Entity\SearchApiPage;
use Drupal\search_api_redisearch\Event\BeforeSearchEvent;

/**
 * Indexes and searches items using the RediSearch.
 *
 * @SearchApiBackend(
 *   id = "search_api_redisearch",
 *   label = @Translation("Redisearch"),
 *   description = @Translation("Use Redisearch as search backend.")
 * )
 */
class RediSearch extends BackendPluginBase implements PluginFormInterface {

  /**
   * The redisearch client connection.
   *
   * @var Setup
   */
  private $client;

  /**
   * The list of custom redisearch dictionaries to use in spellchecking.
   *
   * @var array
   */
  protected static $dictionaries = [];

  use MessengerTrait;
  use PluginFormTrait;

  /**
   * Adds dictionary name to use in this class' spellchecking.
   *
   * @param string $name
   *   The name of the existing redisearch dictionary.
   */
  public static function addDictionary( string $name ) {
    static::$dictionaries[] = $name;
  }

  /**
   * {@inheritdoc}
   *
   * @todo: Implement autocomplete
   */
  public function getSupportedFeatures() {
    return [
      // 'search_api_autocomplete',
      'search_api_facets',
      'search_api_facets_operator_or',
      'search_api_spellcheck',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['redis_host'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Redis Server host'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['redis_host'],
    ];
    $form['redis_port'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Redis Server port'),
      '#required'      => TRUE,
      '#default_value' => $this->configuration['redis_port'],
    ];
    $form['redis_password'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Redis Server password'),
      '#required'      => FALSE,
      '#default_value' => $this->configuration['redis_password'],
    ];
    $form['redis_database'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Redis Server database'),
      '#required'      => FALSE,
      '#default_value' => $this->configuration['redis_database'],
    ];
    $form['redis_spellcheck_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Spellchecking enabled'),
      '#required'      => FALSE,
      '#default_value' => $this->configuration['redis_spellcheck_enabled'],
    ];
    $form['redis_spellcheck_distance'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Spellchecking distance'),
      '#required'      => FALSE,
      '#max'           => 4,
      '#default_value' => $this->configuration['redis_spellcheck_distance'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'redis_host'                 => '127.0.0.1',
      'redis_port'                 => 6379,
      'redis_password'             => NULL,
      'redis_database'             => 0,
      'redis_spellcheck_enabled'   => 0,
      'redis_spellcheck_distance'  => 2,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $distance = $form_state->getValues()['redis_spellcheck_distance'];
    if (!preg_match('/^[1-4]$/', $distance)) {
      $el = $form['redis_spellcheck_distance'];
      $form_state->setError($el, $el['#title'] . ': ' . $this->t('The distance needs to be between 1 and 4'));
    }
  }

  /**
   * Adds index to Redisearch.
   *
   * @param Drupal\search_api\IndexInterface $index
   *   The index.
   */
  public function addIndex(IndexInterface $index) {
    self::updateIndex($index);
  }

  /**
   * Updates the index in Redisearch.
   *
   * All non-fulltext fields will be created as SORTABLE.
   *
   * This is done to allow use of redisearch aggregations without using LOAD which greatly hurts the performance of aggregation query.
   *
   * @param Drupal\search_api\IndexInterface $index
   *   The index.
   */
  public function updateIndex(IndexInterface $index) {
    // Connect to Redis Server and set 'client'.
    if ($this->connect()) {
      // Add index fields.
      $fields = $index->getFields();
      if (!empty($fields)) {
        // Initiate Redis Index.
        $rindex = new Index($this->client);
        // Set index name.
        $rindex->setIndexName($this->setIndexName($index->id()));

        /**
         * Introduced in version 2.0. Tells redisearch which Redis datatype to index.
         * For now, only HASH supported and it can handle almost all our usecases.
         *
         * since: 2.0
         */
        $rindex->on('HASH');
        /**
         * In the RediSearch version 2.0 HASH prefixes tells the engine to which set of data to index.
         * For our purpose, we can use DataSources
         *
         * since: 2.0
         */
        $prefixes = array();
        foreach ($index->getDatasources() as $key => $value ) {
          $prefixes[] = $this->setIndexName($value->getPluginId());
        }
        $rindex->setPrefix($prefixes);
        // Setting a field name for score so we don't need to parse index info everytime
        $rindex->setScoreField('documentScore');
        // Delete the index.
        $rindex->drop();
        // Loop through the fields.
        foreach ($fields as $name => $field) {
          $type = $field->getType();
          $type_fulltext = $type == 'text';
          if (!empty($type)) {
            switch ($type) {
              case 'integer':
              case 'decimal':
                $rindex->addNumericField($name, TRUE);
                break;

              default:
                $weight = '1.0';
                if (!empty($field->getBoost())) {
                  $weight = $field->getBoost();
                }
                $rindex->addTextField($name, $weight, $type_fulltext ? FALSE : TRUE);
            }
          }
        }
        // Save/Create the Index.
        $rindex->create();
      }
    }
  }

  /**
   * Remove index from redisearch.
   *
   * @param mixed $index
   *   The index.
   */
  public function removeIndex($index) {
    if ($this->connect()) {
      // Delete the index.
      $rindex = new Index($this->client);
      $rindex->setIndexName($this->setIndexName($index->id()));
      $rindex->drop();
    }
  }

  /**
   * Add items to index.
   *
   * @param Drupal\search_api\IndexInterface $index
   *   The index.
   * @param array $items
   *   List of items to index.
   *
   * @return array
   */
  public function indexItems(IndexInterface $index, array $items) {
    if ($this->connect()) {
      $rindex = new Index($this->client);
      $rindex->setIndexName($this->setIndexName($index->id()));
      $indexed = [];
      $fields = $index->getFields();
      $fields += $this->getSpecialFields($index);
      foreach ($items as $id => $item) {
        try {
          $data = [];
          foreach ($fields as $fid => $field) {
            if ($fid == 'search_api_id') {
              $data[$fid] = $id;
            }
            else {
              $field_entity = $item->getField($fid);
              if (!empty($field_entity)) {
                $data[$fid] = implode(' ', $field_entity->getValues());
              }
            }
          }
          $data['documentScore'] = $item->getBoost();

          $document = new Document($this->setIndexName($id));
          $document->setFields($data);
          $rindex->delete($id);
          $rindex->add($document);
          $indexed[] = $id;
        }
        catch (\Exception $e) {
          throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
        }
      }
      $rindex->writeToDisk();
      return $indexed;
    }
  }

  /**
   * Delete items from index by their ids.
   *
   * @param Drupal\search_api\IndexInterface $index
   *   The index.
   * @param array $item_ids
   *   List of item ids.
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    if ($this->connect()) {
      $rindex = new Index($this->client);
      $rindex->setIndexName($this->setIndexName($index->id()));
      foreach ($item_ids as $id) {
        $rindex->delete($id);
      }
    }
  }

  /**
   * Delete all items from index.
   *
   * @param Drupal\search_api\IndexInterface $index
   *   The index.
   * @param mixed $datasource_id
   *   Id of the datasource.
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    if ($this->connect()) {
      // Delete the index.
      $rindex = new Index($this->client);
      $rindex->setIndexName($this->setIndexName($index->id()));

      $rindex->drop(TRUE);
      // Recreate the Index back.
      $this->updateIndex($index);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    if ($this->connect() && $index = $query->getIndex()->id()) {
      $index = $this->setIndexName($index);

      $search_id = explode(':', $query->getSearchId());
      if ($search_id[0] == 'search_api_page') {
        $search_api_page = SearchApiPage::load($search_id[1]);
        $show_all_results = $search_api_page->showAllResultsWhenNoSearchIsPerformed();
      }
      else {
        // This should be controlled by search field's required state
        // setting this to FALSE causes counting queries failures.
        $show_all_results = TRUE;
      }

      if (($search_data = $query->getKeys()) || $show_all_results) {
        $rquery = new Query($this->client, $index);
        // Extra data to attach to search result.
        $extra_data = [];

        $redis_params = [
          'allOnEmpty' => $show_all_results,
        ];
        $redis_query = new QueryBuilder($redis_params);
        // Search string for full-text search.
        if (is_array($search_data) || $search_data instanceof QueryBuilder) {
          $conj = 'AND';
          if (is_array($search_data) && isset($search_data['#conjunction'])) {
            $conj = $search_data['#conjunction'];
            unset($search_data['#conjunction']);
          }
          else {
            $search_data = [$search_data];
          }

          // Build QueryBuilder objects that came from search api processors.
          // (use subquery to preserve default AND conjunction in main query)
          $subquery = new QueryBuilder(['conjunction' => $conj]);
          foreach ($search_data as $i => $term) {
            if ($term instanceof QueryBuilder) {
              $subquery->addSubcondition($term);
            }
            else {
              $subquery->addGenericCondition((array) $term, $conj);
            }
          }
          $redis_query->addSubcondition($subquery);
        }
        else {
          // Create very basic query with the search phrase passed as-is.
          $redis_query->addGenericCondition((array) $search_data);
        }

        // Query options.
        $query_options = $query->getOptions();

        // Facets.
        if (!empty($query_options['search_api_facets'])) {
          $extra_data = array_merge($extra_data, $this->getFacets($query_options['search_api_facets'], $redis_query, $rquery));
        }

        // Spell checking.
        if ($this->configuration['redis_spellcheck_enabled'] && empty($query_options['search_api_spellcheck'])) {
          $origkeys = $query->getOriginalQuery()->getKeys();
          $query_options['search_api_spellcheck'] = [
            'keys' => array_filter((array) $origkeys, 'is_int', ARRAY_FILTER_USE_KEY),
            'count' => 1,
            'collate' => TRUE,
          ];
        }

        if (!empty($query_options['search_api_spellcheck'])) {
          $extra_data = array_merge($extra_data, $this->checkSpelling($query_options['search_api_spellcheck'], $rquery));
        }

        $filter = $query->getConditionGroup();
        if ($filter) {
          $redis_query->addSubcondition(QueryBuilder::fromConditionGroup($filter, $redis_params));
        }

        // Pagination.
        $offset = isset($query_options['offset']) ? $query_options['offset'] : 0;
        $limit  = isset($query_options['limit']) ? $query_options['limit'] : 10;
        $rquery->limit($offset, $limit);

        // Fire a BeforeSearchEvent query.
        $dispatcher = Drupal::service('event_dispatcher');
        $event = new BeforeSearchEvent($redis_query);
        $dispatcher->dispatch(BeforeSearchEvent::EVENT_NAME, $event);
        $redis_query = $event->getQuery();

        $search    = $rquery->search($redis_query);
        $documents = $search->getDocuments();
        $results   = $query->getResults();
        $results->setResultCount($search->getCount());
        if (!empty($documents)) {
          foreach ($documents as $doc) {
            $item = $this->getFieldsHelper()
              ->createItem($query->getIndex(), $doc->search_api_id ?? $doc->id);
            $results->addResultItem($item);
          }
        }
        foreach ($extra_data as $key => $array) {
          $results->setExtraData($key, $array);
        }
        return $results;
      }
    }
  }

  /**
   * Check search index's Redis server connection.
   *
   * @return bool
   *   The connection status.
   */
  private function connect() {
    try {
      $this->client = Setup::connect(
        $this->configuration['redis_host'],
        $this->configuration['redis_port'],
        empty($this->configuration['redis_password']) ? NULL : $this->configuration['redis_password'],
        $this->configuration['redis_database']
      );
      return TRUE;
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Retreive facets from redis.
   *
   * @param array $requested
   *   The requested facets data.
   * @param QueryBuilder $redis_query
   *   Current query as a query builder object.
   * @param Query $rquery
   *   Constructed redis query.
   *
   * @return array
   *   Extra data to be applied to query result.
   */
  protected function getFacets(array $requested, QueryBuilder $redis_query, Query $rquery) {
    $returns = [];
    foreach ($requested as $facet_id => $facet_data) {
      if (empty($facet_data['field'])) {
        \Drupal::logger('search_api_redisearch')->warning("Empty facet field for facet $facet_id");
        continue;
      }

      // Filter results for facet field value.
      $field = $facet_data['field'];
      // Try-catch for FT.AGGREGATE 'no results' error from redisearch.
      try {
        $result = $rquery->groupBy([$field])
          ->reduce(['count' => ['count']])
          ->aggregate($redis_query);
      }
      catch (\UnexpectedValueException $e) {
        if (strpos($e->getMessage(), 'no results') !== FALSE) {
          return [];
        }
        else {
          throw $e;
        }
      }

      $count = 0;
      if (!empty($result->getDocuments())) {
        foreach ($result->getDocuments() as $document) {
          // Do not exceed the desired limit.
          $limit_ok = empty($facet_data['limit']) || $count < $facet_data['limit'];
          // Add facet to extra query data if it has more results than minimum.
          $count_ok = !isset($facet_data['min_count']) || $document->count >= $facet_data['min_count'];
          $missing = strlen($document->$field) == 0;
          $missing_ok = !$missing || !empty($facet_data['missing']);

          if ($limit_ok && $count_ok && $missing_ok) {
            $field_filter = $missing ? '!' : '"' . $document->$field . '"';
            $returns['search_api_facets'][$facet_id][] = [
              'filter' => $field_filter,
              'count' => $document->count,
            ];
            $count += 1;
          }
        }
      }
    }
    return $returns;
  }

  /**
   * Retrieve spellcheck from redis.
   *
   * @param array $requested
   *   The requested spelling check data.
   * @param Query $redis_query
   *   Constructed redis query.
   *
   * @return array
   *   Extra data to be merged with query result.
   */
  protected function checkSpelling(array $requested, Query $redis_query) {
    $returns = [];
    $search_data = (array) $requested['keys'];
    $tokenized = [];
    foreach ($search_data as $term) {
      $tokenized += explode(' ', $term);
    }
    $search_data = $tokenized;
    // Escape all special characters in query.
    $special_chars = preg_replace('/[\w\s]/u', '', implode('', $search_data));
    $query = new QueryBuilder(['escape' => $special_chars, 'tokenize' => TRUE]);
    $query->addGenericCondition((array) $search_data);

    // Construct terms array (custom dictionaries)
    $terms = [];
    foreach (static::$dictionaries as $dict) {
      $terms[$dict] = TRUE;
    }

    $result = $redis_query->distance($this->configuration['redis_spellcheck_distance'])
      ->terms($terms)
      ->spellcheck($query);

    $documents = $result->getDocuments();
    if ($documents) {
      if (!empty($requested['collate'])) {
        // Collate - compose spellcheck suggestion from best suggestions of each term.
        $tmp = [];
        foreach ($search_data as $term) {
          $tmp[$term] = $term;
        }
        foreach ($documents as $doc) {
          if (count($doc->suggestions) > 0 && isset($tmp[$doc->term])) {
            $tmp[$doc->term] = array_keys($doc->suggestions)[0];
          }
        }
        $returns = ['collation' => implode(' ', array_values($tmp))];
      }
      else {
        foreach ($documents as $doc) {
          $tmp = array_keys($doc->suggestions);
          $returns['suggestions'][$doc->term] = array_slice($tmp, 0, $requested['count']);
        }
      }
    }
    return ['search_api_spellcheck' => $returns];
  }

  /**
   * To prevent conflict between sites with the same content name on the same Redis server,
   * we prefix index name with sites domain
   *
   * @param string $indexName
   * @return string
   */
  private function setIndexName(string $indexName):string {
    $host = \Drupal::request()->getHost();
    $host = str_replace('.', '', $host);
    return "$host-$indexName";
  }

  /**
   * @inheritDoc
   */
  public function viewSettings(): array {
    $startTime = microtime(true);
    $info[] = [
      'label' => $this->t('RediSearch server URL'),
      'info'  => $this->configuration['redis_host'] . ':' . $this->configuration['redis_port']
    ];

    if ($this->connect() === TRUE) {
      $info[] = [
        'label' => $this->t('Redis server ping latency'),
        'info'  => round( (microtime(true) - $startTime) * 1000, 2) . 'ms'
      ];

      $re = '/(redis_version:?:([0-9\.]{5,8}))/m';
      $redisInfo = $this->client->rawCommand( 'info', [] );
      preg_match_all($re, $redisInfo, $matches, PREG_SET_ORDER, 0);

      $info[] = [
        'label' => $this->t('Redis version'),
        'info'  => $matches[0][2]
      ];

      $modules = $this->client->rawCommand( 'MODULE', ['LIST'] );
      $info[] = [
        'label' => $this->t('RediSearch version'),
        'info'  => $modules[0][3]
      ];

     } else {
       $info[] = [
         'label'  => $this->t('Server connection'),
         'info'   => $this->t('Not connected')
       ];
     }

    return $info;

  }

}
