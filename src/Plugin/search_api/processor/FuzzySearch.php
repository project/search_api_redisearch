<?php

namespace Drupal\search_api_redisearch\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_redisearch\Utility\QueryBuilder;

/**
 * Enable fuzzy matching for redisearch.
 *
 * @SearchApiProcessor(
 *   id = "fuzzy_search",
 *   label = @Translation("Fuzzy search"),
 *   description = @Translation("Adds fuzzy matching to redis search term. Should be applied just before the query."),
 *   stages = {
 *     "preprocess_query" = 100,
 *   }
 * )
 */
class FuzzySearch extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration += [
      'levenshtein_distance' => '1',
    ];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['levenshtein_distance'] = [
      '#type' => 'number',
      '#title' => $this->t('Levenshtein distance'),
      '#description' => $this->t('The maxmimum distance of fuzzy search, that is a maximum character difference between search term and search results'),
      '#default_value' => $this->configuration['levenshtein_distance'],
      '#max' => 3,
    ];$form['fuzzy_weight_difference'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight difference in fuzzy and exact match'),
      '#description' => $this->t('This is useful in sorting search results by giving more priority to results containing the exact word/phrase over fuzzy one. In theory, the number can be as high as you wish. Just play with this till you find good balance.'),
      '#default_value' => $this->configuration['fuzzy_weight_difference'] ?? 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $distance = $form_state->getValues()['levenshtein_distance'];
    if (!preg_match('/^[0-3]$/', $distance)) {
      $el = $form['levenshtein_distance'];
      $form_state->setError($el, $el['#title'] . ': ' . $this->t('The distance needs to be between 0 and 3'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $distance = $this->configuration['levenshtein_distance'];
    $difference = $this->configuration['fuzzy_weight_difference'] ?? 100;
    if ($value instanceof QueryBuilder) {
      $value->setFuzzyMatching($distance);
    }
    else {
      $old_value = $value;
      $value = new QueryBuilder(['fuzzyMatching' => $distance]);
      $value->addGenericCondition((array) $old_value);
    }
  }

}
