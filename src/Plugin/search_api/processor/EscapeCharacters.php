<?php

namespace Drupal\search_api_redisearch\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_redisearch\Utility\QueryBuilder;

/**
 * Enable escaping of tokenizing characters in redisearch.
 *
 * @SearchApiProcessor(
 *   id = "escape_characters",
 *   label = @Translation("Escape characters"),
 *   description = @Translation("Escape characters which cause tokenization in redisearch"),
 *   stages = {
 *     "preprocess_index" = 0,
 *     "preprocess_query" = 0
 *   }
 * )
 */
class EscapeCharacters extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration += [
      'character_set' => ',.<>{}[]"\':;!@#$%^&*()-+=~',
    ];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['character_set'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Characters to be escaped'),
      '#description' => $this->t('All characters listed in the field will be escaped before tokenization and will not cause breaking text into terms.'),
      '#default_value' => $this->configuration['character_set'],
      '#maxlength' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $character_set = $form_state->getValues()['character_set'];
    if (preg_match('/[\w\s]/u', $character_set)) {
      $el = $form['character_set'];
      $form_state->setError($el, $el['#title'] . ': ' . $this->t('Only specials characters other than underscore (_) are allowed'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $character_set = $this->configuration['character_set'];
    if ($value instanceof QueryBuilder) {
      $value->setEscape($character_set);
    }
    else {
      $character_set = str_split($character_set);
      $character_replacements = array_map(function ($x) {
        return "\\$x";
      }, $character_set);
      $value = str_replace($character_set, $character_replacements, $value);
    }
  }

}
