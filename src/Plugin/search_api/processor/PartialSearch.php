<?php

namespace Drupal\search_api_redisearch\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\search_api_redisearch\Utility\QueryBuilder;

/**
 * Enable partial matching for redisearch.
 *
 * @SearchApiProcessor(
 *   id = "partial_search",
 *   label = @Translation("Partial search"),
 *   description = @Translation("Adds prefix matching to redis search term. Should be applied just before the query."),
 *   stages = {
 *     "preprocess_query" = 100,
 *   }
 * )
 */
class PartialSearch extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    if ($value instanceof QueryBuilder) {
      $value->setPrefixMatching(TRUE);
    }
    else {
      $old_value = $value;
      $value = new QueryBuilder(['prefixMatching' => TRUE]);
      $value->addGenericCondition((array) $old_value);
    }
  }

}
