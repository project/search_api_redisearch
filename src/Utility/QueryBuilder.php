<?php

namespace Drupal\search_api_redisearch\Utility;

use FKRediSearch\Query\QueryBuilder as QueryBuilderBase;
use Drupal\search_api\Query\ConditionInterface;
use Drupal\search_api\Query\ConditionGroupInterface;

/**
 * Adds Drupal search_api awareness to QueryBuilder.
 */
class QueryBuilder extends QueryBuilderBase {

  /**
   * Creates QueryBuilder instance from ConditionGroupInterface instance.
   *
   * @param ConditionGroupInterface $condition
   *   Drupal search api condition.
   * @param string[] $params
   *   A list of parameters passed to QueryBuilder constructor.
   *
   * @return QueryBuilder A new object made from $condition.
   *   A new object made from $condition.
   */
  public static function fromConditionGroup(ConditionGroupInterface $condition, array $params = []) {
    $params["conjunction"] = $condition->getConjunction();
    $object = new static($params);
    foreach ($condition->getConditions() as $subcondition) {

      if ($subcondition instanceof ConditionGroupInterface) {
        // Recurse into nested groups.
        $object->addSubcondition(static::fromConditionGroup($subcondition, $params));
      }
      elseif ($subcondition instanceof ConditionInterface) {
        // Conditions are simple field conditions.
        $field = $subcondition->getField();
        $value = $subcondition->getValue();
        $operator = $subcondition->getOperator();

        // Filter as much as possible - discard unsupported operators.
        if ($operator == '=') {
          $object->addCondition($field, (array) $value, 'AND', TRUE);
        }
      }
    }

    return $object;
  }

}
